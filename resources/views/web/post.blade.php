@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-8 offset-2">
            <h1>{{ $post->name }}</h1>
            <div class="card">
                <div class="card-header">
                    Creado Por:
                    {{$post->user->name }}
                </div>
                <div class="card-body">
                    @if($post->file)
                        <img src="{{ $post->file }}" class="img-fluid">
                    @endif
                    {{ $post->excerpt }}
                        <hr>
                    {!! $post->body !!}
                </div>
            </div>
        </div>
    </div>
@endsection
